/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hr.fer.streamslab.gsd;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.atomic.AtomicInteger;

import hr.fer.streamslab.gsd.loader.LineStringDataLoader;
import hr.fer.streamslab.gsd.loader.PointDataLoader;
import hr.fer.streamslab.gsd.loader.PolygonDataLoader;
import hr.fer.streamslab.gsd.serializer.impl.ApacheSerializer;
import hr.fer.streamslab.gsd.serializer.impl.GeoToolsSerializer;
import hr.fer.streamslab.gsd.serializer.impl.GsonSerializer;
import hr.fer.streamslab.gsd.serializer.impl.JacksonAfterburnerSerializer;
import hr.fer.streamslab.gsd.serializer.impl.JacksonSerializer;
import hr.fer.streamslab.gsd.serializer.impl.JavaSerializer;
import hr.fer.streamslab.gsd.serializer.impl.KryoSerializer;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.PrecisionModel;

/**
 *
 * @author kpripuzic
 */
public class Main {

    public static void main(String[] args) throws NoSuchElementException, IOException {
        //presision of 10 decimal digits
        int decimals = 10;
        GeometryFactory gf = new GeometryFactory(new PrecisionModel(Math.pow(10, decimals)), 4326);

//        //load areas
//        String filePath = "../geofil/GB_Postcodes-QGIS_fixed/PostalArea_fixed.shp";
//        List<Geometry> areas = PolygonDataLoader.loadShapeFile(filePath, gf);
//        System.out.println("Areas: " + areas.size());
//
//        //load districts
//        filePath = "../geofil/GB_Postcodes-QGIS_fixed/PostalDistrict_fixed.shp";
//        List<Geometry> districts = PolygonDataLoader.loadShapeFile(filePath, gf);
//        System.out.println("Districts: " + districts.size());
//
//        //load sectors
//        filePath = "../geofil/GB_Postcodes-QGIS_fixed/PostalSector_fixed.shp";
//        List<Geometry> sectors = PolygonDataLoader.loadShapeFile(filePath, gf);
//        System.out.println("Sectors: " + sectors.size());

//        List<Geometry> polygons = Stream.concat(
//                Stream.concat(areas.stream(), districts.stream()),
//                sectors.stream()).collect(Collectors.toList());
//        System.out.println("Polygons: " + polygons.size());
        

        final GsonSerializer gsonSerializer = new GsonSerializer();
        final JavaSerializer javaSerializer = new JavaSerializer();
        final JacksonSerializer jacksonSerializer = new JacksonSerializer();
        final JacksonAfterburnerSerializer jacksonAfterburnerSerializer = new JacksonAfterburnerSerializer();
        final GeoToolsSerializer geoToolsSerializer = new GeoToolsSerializer(gf);
        final ApacheSerializer apacheSerializer = new ApacheSerializer();

        for (int i = 0; i < 3; i++) {
            List<Geometry> list;

            if (i == 0) {
                //load points
                String filePath = "points/Accidents0515.csv";
                System.out.println("Points loading");
                list = PointDataLoader.loadCsvFile(filePath, gf);
            } else if (i==1) {
                //load linestrings
                String dirPath = "linestrings";
                File folder = new File(dirPath);
                File[] listOfFiles = folder.listFiles();
                System.out.println("LineStrings loading");
                list = new LinkedList<>();
                AtomicInteger discardedCounter = new AtomicInteger(0);
                for (File file : listOfFiles) {
                    if (list.size()>=1000000) {
                        list = list.stream().limit(1000000).toList();
                        break;
                    }
                    if (file.isFile() && file.getName().endsWith(".shp")) {
                        List<Geometry> lineStringsPart = LineStringDataLoader.loadShapeFile(dirPath + "/" + file.getName(), gf, discardedCounter);
                        list.addAll(lineStringsPart);
                    }
                }

                if (discardedCounter.get() != 0) {
                    System.out.println("Discarded a total of " + discardedCounter + " geometries for not matching LineString type");
                }
            } else {
                //load woodlands
                String dirPath = "polygons";
                File folder = new File(dirPath);
                File[] listOfFiles = folder.listFiles();
                System.out.println("Polygons loading");
                list = new LinkedList<>();
                AtomicInteger discardedCounter = new AtomicInteger(0);
                for (File file : listOfFiles) {
                    if (list.size()>=1000000) {
                        list = list.stream().limit(1000000).toList();
                        break;
                    }
                    if (file.isFile() && file.getName().endsWith("Woodland.shp")) {
                        List<Geometry> woodlandsPart = PolygonDataLoader.loadShapeFile(dirPath + "/" + file.getName(), gf, discardedCounter);
                        list.addAll(woodlandsPart);
                    }
                }
                if (discardedCounter.get() != 0) {
                    System.out.println("Discarded a total of " + discardedCounter + " geometries for not matching Polygon type");
                }
            }

            final KryoSerializer kryoSerializer = new KryoSerializer((list.get(0)).getClass(), 12L);
            
            System.out.println("Single-threaded test:\n");
            System.out.println("Total Geometry objects: " + list.size() + "\n");
            System.out.println("java: " + javaSerializer.test(list, 10, false) + "\n");
            System.out.println("apache: " + apacheSerializer.test(list, 10,false) + "\n");
            System.out.println("Gson: " + gsonSerializer.test(list, 10,false) + "\n");
            System.out.println("kryo: " + kryoSerializer.test(list, 10,false) + "\n");
            System.out.println("jackson: " + jacksonSerializer.test(list, 10,false) + "\n");
            System.out.println("jackson + afterburner: " + jacksonAfterburnerSerializer.test(list, 10,false) + "\n");
            System.out.println("geotools: " + geoToolsSerializer.test(list, 10,false) + "\n");


            System.out.println("Multi-threaded test:\n");
            System.out.println("java: " + javaSerializer.test(list, 10, true) + "\n");
            System.out.println("apache: " + apacheSerializer.test(list, 10,true) + "\n");
            System.out.println("Gson: " + gsonSerializer.test(list, 10,true) + "\n");
            System.out.println("kryo: " + kryoSerializer.test(list, 10,true) + "\n");
            System.out.println("jackson: " + jacksonSerializer.test(list, 10,true) + "\n");
            System.out.println("jackson + afterburner: " + jacksonAfterburnerSerializer.test(list, 10,true) + "\n");
            System.out.println("geotools: " + geoToolsSerializer.test(list, 10,true) + "\n");

            System.gc();
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }


    }
}
