package hr.fer.streamslab.gsd.loader;

import org.geotools.data.shapefile.ShapefileDataStore;
import org.geotools.data.simple.SimpleFeatureIterator;
import org.geotools.data.store.ContentFeatureCollection;
import org.geotools.data.store.ContentFeatureSource;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.precision.GeometryPrecisionReducer;
import org.opengis.feature.simple.SimpleFeature;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.atomic.AtomicInteger;


public class LineStringDataLoader {
    public static List<Geometry> loadShapeFile(String filePath, GeometryFactory gf, AtomicInteger discardedCounter) throws MalformedURLException, NoSuchElementException, IOException {
        //sectors
        List<Geometry> result = new LinkedList<>();
        File file = new File(filePath);
        ShapefileDataStore dataStore = new ShapefileDataStore(file.toURI().toURL());
        ContentFeatureSource featureSource = dataStore.getFeatureSource();
        ContentFeatureCollection featureCollection = featureSource.getFeatures();
        try (SimpleFeatureIterator iterator = featureCollection.features()) {
            while (iterator.hasNext()) {
                SimpleFeature feature = iterator.next();
                GeometryPrecisionReducer gpr = new GeometryPrecisionReducer(gf.getPrecisionModel());
                Geometry originalGeometry = (Geometry) feature.getDefaultGeometry();
                //reducing the number of decimal digits
                Geometry geometry = (Geometry) gpr.reduce(originalGeometry);
                if (geometry.getClass() == LineString.class) {
                    result.add(geometry);
                } else {
                    discardedCounter.incrementAndGet();
                }
            }
        }
        dataStore.dispose();
        return result;
    }
}
