package hr.fer.streamslab.gsd.loader;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.stream.Stream;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;

public class PointDataLoader {

    public static ArrayList<Geometry> loadCsvFile(String filePath, GeometryFactory gf) throws FileNotFoundException, IOException {
        ArrayList<Geometry> result = null;
        try ( Stream<String> stream = Files.lines(Paths.get(filePath))) {
            // skip first line
            result = stream.map(line -> line.split(",")).
                    filter(splittedLine -> isSpittedLineValid(splittedLine)).
                    limit(1000000).
                    map(splittedLine -> gf.createPoint(new Coordinate(Double.parseDouble(splittedLine[3]),
                    Double.parseDouble(splittedLine[4])))).
                    collect(ArrayList::new, ArrayList::add, ArrayList::addAll);
        }

        return result;
    }

    private static boolean isSpittedLineValid(String[] splittedLine) {
        try {
            Double.parseDouble(splittedLine[3]);
            Double.parseDouble(splittedLine[4]);
            return true;
        } catch (NumberFormatException ex) {
            return false;
        }
    }
}
