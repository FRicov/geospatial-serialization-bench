package hr.fer.streamslab.gsd.serializer;

import org.locationtech.jts.geom.Geometry;

import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public abstract class AbstractSerializer {

    public String test(final List<Geometry> objectsToSerialize, final int iterations, final boolean multiThreaded) {
        final StringBuilder stringBuilder = new StringBuilder();

        List<byte[]> serializedType = new ArrayList<>();
        final Duration[] serDuration = new Duration[iterations];
        Duration serDurationSum = Duration.ZERO;

        //warm-up phase
        for (Geometry geometry : objectsToSerialize) {
            try {
                serialize(geometry);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        for (int i = 0; i < iterations; i++) {
            serializedType = null;
            System.gc();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            final Instant serStart = Instant.now();
            final Stream<Geometry> stream = multiThreaded ? objectsToSerialize.stream().parallel() : objectsToSerialize.stream();
            serializedType = stream.map((object) -> {
                try {
                    return serialize(object);
                } catch (IOException e) {
                    e.printStackTrace();
                    return null;
                }
            }).collect(Collectors.toList());

            final Instant serEnd = Instant.now();

            serDuration[i] = Duration.between(serStart, serEnd);
            serDurationSum = serDurationSum.plus(serDuration[i]);
        }
        final Duration avgSerDuration = serDurationSum.dividedBy(iterations);
        final long[] serDurationMilis = Arrays.stream(serDuration).mapToLong(Duration::toMillis).toArray();
        final double serVariance = Arrays.stream(serDurationMilis)
                .mapToDouble(duration -> Math.pow((double) (duration - avgSerDuration.toMillis()), 2))
                .sum() / iterations;
        final long serStdDev = Math.round(Math.sqrt(serVariance));

        stringBuilder
                .append("Serialization duration per iteration (ms): ")
                .append(Arrays.toString(serDurationMilis))
                .append("\n")
                .append("Average serialization time (ms): ")
                .append(avgSerDuration.toMillis())
                .append("\n")
                .append("Serialization standard deviation (ms): ")
                .append(serStdDev)
                .append("\n");

        List<Geometry> deserializedObjects = new ArrayList<>();
        final Duration[] deserDuration = new Duration[iterations];

        //warm-up phase
        for (byte[] value : serializedType) {
            try {
                deserialize(value);
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        Duration deserDurationSum = Duration.ZERO;
        for (int i = 0; i < iterations; i++) {
            deserializedObjects = null;
            System.gc();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            final Instant deserStart = Instant.now();
            final Stream<byte[]> stream = multiThreaded ? serializedType.stream().parallel() : serializedType.stream();
            deserializedObjects = stream.map((object) -> {
                try {
                    return deserialize(object);
                } catch (IOException | ClassNotFoundException e) {
                    e.printStackTrace();
                    return null;
                }
            }).collect(Collectors.toList());

            final Instant deserEnd = Instant.now();

            deserDuration[i] = Duration.between(deserStart, deserEnd);
            deserDurationSum = deserDurationSum.plus(deserDuration[i]);
        }
        final Duration avgDeserDuration = deserDurationSum.dividedBy(iterations);
        final long[] deserDurationMilis = Arrays.stream(deserDuration).mapToLong(Duration::toMillis).toArray();
        final double deserVariance = Arrays.stream(deserDurationMilis)
                .mapToDouble(duration -> Math.pow((double) (duration - avgDeserDuration.toMillis()), 2))
                .sum() / iterations;
        final long deserStdDev = Math.round(Math.sqrt(deserVariance));

        boolean flag = true;
        for (int i = 0; i < objectsToSerialize.size(); i++) {
            final Geometry objTS = objectsToSerialize.get(i);
            final Geometry dObj = deserializedObjects.get(i);
            if (!objTS.equals(dObj)) {
                flag = false;
                break;
            }
        }

        long sizeSum = 0L;
        for (final byte[] bytes : serializedType) {
            sizeSum += bytes.length;
        }

        stringBuilder
                .append("Deserialization duration per iteration (ms): ")
                .append(Arrays.toString(deserDurationMilis))
                .append("\n")
                .append("Average deserialization time (ms): ")
                .append(avgDeserDuration.toMillis())
                .append("\n")
                .append("Deserialization standard deviation (ms): ")
                .append(deserStdDev)
                .append("\n")
                .append("Total average serialization and deserialization time (ms): ")
                .append(avgSerDuration.plus(avgDeserDuration).toMillis())
                .append("\n")
                .append("Total size after deserialization (bytes): ").append(sizeSum).append("\n")
                .append(flag ? "Success" : "Failure");

        return stringBuilder.toString();
    }


    public abstract byte[] serialize(Geometry o) throws IOException;

    public abstract Geometry deserialize(byte[] s) throws IOException, ClassNotFoundException;

}
