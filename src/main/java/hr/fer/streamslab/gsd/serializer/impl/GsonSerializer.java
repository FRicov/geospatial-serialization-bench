package hr.fer.streamslab.gsd.serializer.impl;

import com.github.filosganga.geogson.gson.GeometryAdapterFactory;
import com.github.filosganga.geogson.jts.JtsAdapterFactory;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.locationtech.jts.geom.Geometry;
import hr.fer.streamslab.gsd.serializer.AbstractSerializer;

import java.nio.charset.StandardCharsets;

public class GsonSerializer extends AbstractSerializer {

    final Gson gson;

    public GsonSerializer() {
        this.gson = new GsonBuilder()
                .registerTypeAdapterFactory(new JtsAdapterFactory())
                .registerTypeAdapterFactory(new GeometryAdapterFactory())
                .create();

    }

    @Override
    public byte[] serialize(final Geometry o) {
        return gson.toJson(o).getBytes(StandardCharsets.UTF_8);
    }

    @Override
    public Geometry deserialize(final byte[] s) {
        return gson.fromJson(new String(s), Geometry.class);
    }

}
