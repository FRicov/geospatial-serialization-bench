package hr.fer.streamslab.gsd.serializer.impl;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import com.esotericsoftware.kryo.util.DefaultInstantiatorStrategy;
import hr.fer.streamslab.gsd.serializer.AbstractSerializer;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.CoordinateXYZM;
import org.locationtech.jts.geom.Envelope;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.geom.LinearRing;
import org.locationtech.jts.geom.MultiPolygon;
import org.locationtech.jts.geom.Point;
import org.locationtech.jts.geom.Polygon;
import org.locationtech.jts.geom.PrecisionModel;
import org.locationtech.jts.geom.impl.CoordinateArraySequence;
import org.locationtech.jts.geom.impl.CoordinateArraySequenceFactory;
import org.objenesis.strategy.StdInstantiatorStrategy;

import java.util.concurrent.LinkedBlockingQueue;

public class KryoSerializer extends AbstractSerializer {

    final LinkedBlockingQueue<Kryo> kryoPool = new LinkedBlockingQueue<>();
    Class<? extends Geometry> type;

    public KryoSerializer(Class<? extends Geometry> type, Long poolInstances) {
        this.type = type;
        for (int i = 0; i < poolInstances; i++) {
            Kryo kryo = new Kryo();
            kryo.register(Polygon.class);
            kryo.register(Polygon[].class);
            kryo.register(MultiPolygon.class);
            kryo.register(Geometry.class);
            kryo.register(Envelope.class);
            kryo.register(LinearRing.class);
            kryo.register(GeometryFactory.class);
            kryo.register(CoordinateArraySequenceFactory.class);
            kryo.register(PrecisionModel.class);
            kryo.register(PrecisionModel.Type.class);
            kryo.register(LinearRing[].class);
            kryo.register(CoordinateArraySequence.class);
            kryo.register(Coordinate[].class);
            kryo.register(Coordinate.class);
            kryo.register(Point.class);
            kryo.register(LineString.class);
            kryo.register(CoordinateXYZM.class);
            kryo.setInstantiatorStrategy(new DefaultInstantiatorStrategy(new StdInstantiatorStrategy()));
            kryoPool.add(kryo);
        }

    }

    @Override
    public byte[] serialize(final Geometry o) {
        Kryo kryo;
        try {
            kryo = kryoPool.take();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        Output output = new Output(256, 786432);
        kryo.writeObject(output, o);
        final byte[] buffer = output.getBuffer();
        output.close();
        kryoPool.add(kryo);
        return buffer;
    }

    @Override
    public Geometry deserialize(final byte[] s) {
        Kryo kryo;
        try {
            kryo = kryoPool.take();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        Input input = new Input(s);
        final Geometry g = kryo.readObject(input, type);

        input.close();
        kryoPool.add(kryo);
        return g;
    }

}
