package hr.fer.streamslab.gsd.serializer.impl;

import com.bedatadriven.jackson.datatype.jts.JtsModule;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.locationtech.jts.geom.Geometry;
import hr.fer.streamslab.gsd.serializer.AbstractSerializer;

import java.io.IOException;

public class JacksonSerializer extends AbstractSerializer {

    ObjectMapper objectMapper = new ObjectMapper();

    public JacksonSerializer() {
        this.objectMapper.registerModule(new JtsModule());
    }

    @Override
    public byte[] serialize(final Geometry o) throws JsonProcessingException {
        return objectMapper.writeValueAsBytes(o);
    }

    @Override
    public Geometry deserialize(final byte[] s) throws IOException {
        return objectMapper.readValue(s, Geometry.class);
    }
}
