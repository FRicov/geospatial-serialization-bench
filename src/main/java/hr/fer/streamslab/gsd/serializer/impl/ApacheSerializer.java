package hr.fer.streamslab.gsd.serializer.impl;

import hr.fer.streamslab.gsd.serializer.AbstractSerializer;
import org.apache.commons.lang3.SerializationUtils;
import org.locationtech.jts.geom.Geometry;

public class ApacheSerializer extends AbstractSerializer {

    @Override
    public byte[] serialize(final Geometry o) {
        return SerializationUtils.serialize(o);
    }

    @Override
    public Geometry deserialize(final byte[] s) {
        return SerializationUtils.deserialize(s);
    }
}
