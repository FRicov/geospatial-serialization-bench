package hr.fer.streamslab.gsd.serializer.impl;

import org.locationtech.jts.geom.Geometry;
import hr.fer.streamslab.gsd.serializer.AbstractSerializer;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;


public class JavaSerializer extends AbstractSerializer{

    @Override
    public byte[] serialize(final Geometry o) throws IOException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutputStream out = new ObjectOutputStream(bos);
        out.writeObject(o);
        byte[] bytes = bos.toByteArray();
        bos.close();
        return bytes;
    }

    @Override
    public Geometry deserialize(final byte[] s) throws IOException, ClassNotFoundException {
        ByteArrayInputStream bis = new ByteArrayInputStream(s);
        ObjectInput in = new ObjectInputStream(bis);
        Object o = in.readObject();
        bis.close();
        return (Geometry) o;
    }
}
